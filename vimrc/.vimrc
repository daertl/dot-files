" Set compatibility to Vim only.
set nocompatible

" Helps force plug-ins to load correctly when it is turned back on below.
filetype off

" Turn on syntax highlighting.
syntax on

" For plug-ins to load correctly.
filetype plugin indent on

call plug#begin('~/.vim/plugged')

Plug 'mhinz/vim-startify'

Plug 'junegunn/fzf.vim'

Plug 'vim-airline/vim-airline'

Plug 'vim-airline/vim-airline-themes'

Plug 'tpope/vim-commentary'

Plug 'tpope/vim-surround'

Plug 'easymotion/vim-easymotion'

Plug 'scrooloose/nerdtree'

Plug 'justmao945/vim-clang'

Plug 'lervag/vimtex'

Plug 'puremourning/vimspector'

Plug 'xavierd/clang_complete'

Plug 'artur-shaik/vim-javacomplete2'

Plug 'tpope/vim-surround'

Plug 'scrooloose/syntastic'

Plug 'honza/vim-snippets'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'ervandew/supertab'

Plug 'vim-airline/vim-airline'

Plug 'dawikur/base16-vim-airline-themes'

Plug 'easymotion/vim-easymotion'

call plug#end()

" Turn on mouse support
set mouse=a

"set spellcheck for englisch and german
set spelllang=en,de
set spell

" Turn off modelines
set modelines=0

set wildmenu

set undofile

" To remove the ending of the line it was $ sign
set list!

" To store it in the main clipboar
set clipboard+=unnamedplus

" Automatically wrap text that extends beyond the screen length.
set wrap
" Vim's auto indentation feature does not work properly with text copied from outside of Vim. Press the <F2> key to toggle paste mode on/off.
nnoremap <F2> :set invpaste paste?<CR>
imap <F2> <C-O>:set invpaste paste?<CR>
set pastetoggle=<F2>
noremap <F4> :wa! <CR> :terminal make<CR>

nmap <C-f> :NERDTreeToggle<CR>

"java-complete config
autocmd FileType java setlocal omnifunc=javacomplete#Complete
nmap <F4> <Plug>(JavaComplete-Imports-AddSmart)
imap <F4> <Plug>(JavaComplete-Imports-AddSmart)

" terminal
"au TerminalOpen * startinsert

" window navigation in terminal mode, also enters normal mode
tmap <silent> <C-w>k <C-\><C-n>:wincmd k<CR>
tmap <silent> <C-w>j <C-\><C-n>:wincmd j<CR>
tmap <silent> <C-w>h <C-\><C-n>:wincmd h<CR>
tmap <silent> <C-w>l <C-\><C-n>:wincmd l<CR>

autocmd FileType tex map <F5> :wa \| !clear && latexmk -pdf -quiet %<cr>

" Maping to use the main clipboard when copying
vnoremap <C-c> "+y
map <C-v> "+p


"Airline config
let g:airline#extensions#tabline#enabled = 1

" Clang config
 " path to directory where library can be found
 "let g:clang_library_path='/usr/lib/llvm-3.8/lib'
 " or path directly to the library file
 "let g:clang_library_path='/usr/lib64/libclang.so.10'

" Uncomment below to set the max textwidth. Use a value corresponding to the width of your screen.
" set textwidth=79o
set formatoptions=tcqrn1
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set noshiftround

" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start

" Speed up scrolling in Vim
set ttyfast

" Status bar
set laststatus=2

" Display options
set showmode
set showcmd

" von Fab config
set noswapfile
set showmatch
set linebreak
set cursorline
set termguicolors


" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

" Display different types of white spaces.
set list
"set listchars=tab:›\ ,trail:•,extends:#,nbsp:.

" Show line numbers
set number
set relativenumber
set scrolloff=5
set tabstop=4
set shiftwidth=4
set softtabstop=4


" Set status line display
" set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%l,%v][%p%%]\ [BUFFER=%n]\ %{strftime('%c')}
function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

set statusline=
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ [%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\

" Encoding
set encoding=utf-8

" Highlight matching search patterns
set hlsearch
" Enable incremental search
set incsearch
" Include matching uppercase words with lowercase search term
set ignorecase
" Include only uppercase words with uppercase search term
set smartcase

" Store info from no more than 100 files at a time, 9999 lines of text, 100kb of data. Useful for copying large amounts of data between files.
set viminfo='100,<9999,s100

" Map the <Space> key to toggle a selected fold opened/closed.
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf

" Map the <F5> to call make
" noremap <F5> :wa! <CR> :terminal make <CR>
"
"Vimspector
let g:vimspector_enable_mappings = 'VISUAL_STUDIO'
"packadd! vimspector


"Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Automatically save and load folds
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview"
