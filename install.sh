#!/usr/bin/env bash

[ -f .git/hooks/post-merge ] || {
    echo "Setup post-merge git hook ..."
    cat <<-EOF > .git/hooks/post-merge
	#!/bin/sh
	$0
	EOF
}

echo "Stowing dot-files ..."

# ensure some directories are not symlinked
mkdir -p "$HOME/.vim/autoload"

# prevent generated files from fucking up my stowing
rm -f "$HOME/.Xresources"
rm -f "$HOME/.config/mimeapps.list"

# shellcheck disable=SC2046,SC2035
stow -v -t "$HOME" $(ls -d */)

