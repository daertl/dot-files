# dot-files
To setup the automaticlly symlink generation, create file in .git named `post-merge` and add the stow command showen below:
## SETUP automatic stowing with git hook
Use the `install.sh` script to setup the automaticlly stowing.
The script adds following contetn into the file `.git/hooks/post-merge`.
```sh
#!/bin/sh

# shellcheck disable=SC2046,SC2035
stow -v -t "$HOME" $(ls -d */)

EOF
```
