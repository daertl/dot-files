id=$(xsetwacom list devices | grep PAD | sed 's/.*id: \([0-9]*\).*/\1/g')
xsetwacom set "$id" Button 1 "key +ctrl z -ctrl"
xsetwacom set "$id" Button 2 "key +ctrl y -ctrl"
xsetwacom set "$id" Button 3 "key +ctrl d -ctrl"
xsetwacom set "$id" Button 4 "key +ctrl s -ctrl"
